# javadoc_generator

#### 介绍
利用javadoc导出word文档

这个是博客地址
https://www.cnblogs.com/chongcheng/p/12969373.html

```
public class Test {
	public static void main(String[] args) throws Exception {
		String jarPath = "C:\\Users\\HongCheng\\Desktop\\11.jar";
		JavaDocReader javaDocReader = new JavaDocReader( "hongcheng.code_generator"
				,"hongcheng.code_generator"
				,Collections.singletonList(jarPath),Modifier.PRIVATE,Modifier.PUBLIC );
		List<ClassComment> execute = javaDocReader.execute();
		
		WordExport wordExport = new WordExport();
		wordExport.export(execute,"C:\\Users\\HongCheng\\Desktop\\1.docx");
	}
}

```

注意： 

1、这个一般是你把你自己要导出文档的项目打成jar包，再用这个工具进行处理，我没试过把这个工具嵌入到当前项目中。

2、当然也可以指定一个java文件，但是它所依赖的一些jar你要加到classpath中。

3、打的jar包必须带有java源码，你可以打成源码jar包。编译后的jar是没有注释的。

4、这个的核心就是借助javadoc去解析java文件