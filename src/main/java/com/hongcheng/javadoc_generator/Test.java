package com.hongcheng.javadoc_generator;

import java.util.Collections;
import java.util.List;

import com.hongcheng.javadoc_generator.entity.ClassComment;

public class Test {
	public static void main(String[] args) throws Exception {
        // 我们自己的源项目jar包
		String jarPath = "C:\\Users\\HongCheng\\Desktop\\11.jar";
        // 构建一个我们自己编写的javadoc读取类
		JavaDocReader javaDocReader = new JavaDocReader( "hongcheng.code_generator"
				,"hongcheng.code_generator"
				,Collections.singletonList(jarPath),Modifier.PRIVATE,Modifier.PUBLIC );
        // 执行读取以获取注释
		List<ClassComment> execute = javaDocReader.execute();
		// 以自己喜欢的方式输入注释
		WordExport wordExport = new WordExport();
		wordExport.export(execute,"C:\\Users\\HongCheng\\Desktop\\1.docx");
	}
}
